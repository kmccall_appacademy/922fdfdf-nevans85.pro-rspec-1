def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  result = 0
  array.each { |el| result += el }
  result
end

def multiply(*nums)
  nums.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  return (1..num).reduce(:*) unless num.zero?
  1
end
