def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, times = 2)
  result = string
  (2..times).each { result += " #{string}" }
  result
end

def start_of_word(string, num_letters)
  string.split('').take(num_letters).join('')
end

def first_word(string)
  string.split.first
end

def titleize(string)
  little_words = %w[a an and the over]
  titleized_words = string.split.map do |e|
    if little_words.include?(e)
      e
    else
      e.capitalize
    end
  end
  titleized_words[0].capitalize!
  titleized_words.join(' ')
end
