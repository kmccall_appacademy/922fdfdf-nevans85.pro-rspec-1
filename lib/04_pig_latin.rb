CONSONANTS = %w[b c d f g h j k l m n p q r s t v w x y z].freeze
def translate(string)
  words = string.split
  words.map! { |word| translate_word(word) }
  words.join(' ')
end

def translate_word(word)
  letters = word.split('')
  letters.rotate! while CONSONANTS.include?(letters[0])
  letters.rotate! if letters.last == 'q'
  word = letters.join('') + 'ay'
  word
end
